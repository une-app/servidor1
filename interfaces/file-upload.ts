

export interface FileUpload{
    name: string;
    data:any;
    encoding:string;
    tempFilePath:string,
    truncated: boolean,
    mimetype: string,

    mv:Function //no es mi función pero lo ponemos en el interfaz para que al usarla en el código no de error.
}