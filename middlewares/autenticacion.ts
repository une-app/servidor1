import { Response, Request, NextFunction} from 'express'
import Token from '../classes/token';
import { Usuario } from '../models/usuario.model'

export const verificaToken = ( req:any, res:Response, next: NextFunction) =>{

    const userToken = req.get('x-token') || '';
    
    
    Token.comprobarToken( userToken )
        .then( (decoded: any) =>{
            req.usuario = decoded.usuario;
            Usuario.findById(req.usuario,(err,usuario) =>{ //por si se ha eliminado el usuario ???
                    if(err){
                        res.json({
                            ok:false,
                            err
                        })
                    }
            });

            next(); //esta funcion indica que la aplicación puede seguir, esta autentificado
        }).catch(err =>{
            res.json({
                ok:false,
                mensaje:'El Token no es correcto'
            });
        });
}