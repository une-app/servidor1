import { Router, Request, Response } from 'express';
import bcrypt from 'bcrypt';

import Token from '../classes/token';
import { Usuario } from '../models/usuario.model';
import { verificaToken } from '../middlewares/autenticacion';



const userRoutes = Router();

//Listado de usuarios
userRoutes.get('/', async (req:Request,res:Response)=>{
            var usuario = await Usuario.find().exec()
                .catch(err=>{
                    res.json({
                        ok:false,
                        err
                    });
                });

            res.json({
                ok:true,
                usuario
            });
});

//Información de mi usuario
userRoutes.get('/yo',[verificaToken],async(req:any,res:Response)=>{
    const usuario = req.usuario;
    const informacion = await Usuario.findById(usuario).exec();

    res.json({
        ok:true,
        informacion
    });
});


//Devolver usuario por id
userRoutes.get('/:id',async(req:any,res:Response)=>{
    const id = req.params.id;
    const usuario = await Usuario
                        .findById(id)
                        .exec();

    res.json({
        ok:true,
        usuario
    })
});

//Login
userRoutes.post('/login',(req: Request, res:Response) =>{

    const body = req.body;
    Usuario.findOne({correo: body.correo}, ( err, userDB)=>{

        if ( err ) throw err;

        if (!userDB){
            return res.json({
                ok:false,
                mensaje:'usuario/contraseña no son correctos no correo'
            });
        }
        
        if(userDB.compararPassword(body.password )){

             const tokenUser = Token.getJwtToken({
                 _id: userDB._id,
                 nombre:userDB.nombre,
                 correo:userDB.correo,
                 foto:userDB.foto
             });
            
            res.json({
                ok:true,
                token: tokenUser
            });

        }else{
            return res.json({
                ok:false,
                mensaje: 'usuario/contraseña no son correctos ***'
            });
        }
    })

});

//Crear un usuario
userRoutes.post('/create', (req: Request,res: Response)=>{
    
    //obtienes la información que entra por el request. lo guardas en la variable user
    const user = {
        nombre: req.body.nombre,
        apellidos:req.body.apellidos,
        correo: req.body.correo,
        password: bcrypt.hashSync(req.body.password,10),
        foto: req.body.foto,
        telefono: req.body.telefono
    }


     // --mongoose documentation--
     /**
     * Shortcut for saving one or more documents to the database. MyModel.create(docs)
     * does new MyModel(doc).save() for every doc in docs.
     * Triggers the save() hook.
     */
    //Guardas la variable en la base de datos y te devuelve userDB (la informacion que esta en la base de datos)
    Usuario.create( user ).then( (userDB: any)=>{
        //Transformas a los usuarios en el token correspondiente.
        const tokenUser = Token.getJwtToken({
            _id: userDB._id,
            nombre:userDB.nombre,
            apellidos:req.body.apellidos,
            correo:userDB.correo,
            foto:userDB.foto,
            localidad:userDB.localidad,
            telefono: req.body.telefono
        });
       //Devuelves el ok y el token del usuario.
       res.json({
           ok:true,
           token: tokenUser
       });

    }).catch( err=>{

            res.json({
                ok:false,
                err
            }); 
        });
    
   
});

// Actualizar usuario
userRoutes.post('/update', [verificaToken], (req:any, res: Response)=>{
    
    const user ={
        nombre:  req.body.nombre || req.usuario.nombre,
        apellidos: req.body.apellidos || req.usuario.apellidos,
        correo:  req.body.correo || req.usuario.correo,
        foto: req.body.foto || req.usuario.foto,
        localidad: req.body.localidad || req.usuario.localidad,
        telefono: req.body.telefono || req.usuario.telefono,
        informacion: req.body.informacion || req.usuario.informacion,
        vehiculos: req.body.vehiculos || req.usuario.vehiculos
    }

    Usuario.findByIdAndUpdate( req.usuario._id, user, { new:true }, (err,userDB)=>{
       
        //si se updatea con un correo o telefono duplicado mandamos el error.
        
         
        
        if( err ){
            if( err.codeName == 'DuplicateKey' ){
                return res.json({
                    ok:false,
                    mensaje: err.errmsg
                })
            }else{ 

            throw err;
            
            };
        };

        if( !userDB ){
            return res.json({
                ok:false,
                mensaje: 'No existe un usuario con ese ID'
            });
        }
    
        const tokenUser = Token.getJwtToken({
            _id: userDB._id,
            nombre:userDB.nombre,
            correo:userDB.correo,
        });
       
       res.json({
           ok:true,
           token: tokenUser
       });

    });
    
});

userRoutes.get('/', [verificaToken], (req:any, res:Response) =>{
    const usuario = req.usuario;
    //si la información es correcta devolvemos la información del usuario
    res.json({
        ok:true,
        usuario
    });
});



export default userRoutes;