import { Router, Response, Request } from "express";
import { verificaToken } from '../middlewares/autenticacion';


import { Usuario } from '../models/usuario.model';
import { Mensaje } from '../models/chat/mensaje.model';
import { Chat } from '../models/chat/chat.model';
import viajeRoutes from "./viaje";



const chatRoutes = Router();


//Listado de mensajes propios de un chat
chatRoutes.get("/mensajes", [verificaToken], async (req:any, res:Response)=>{

    const id_chat = req.body.id_chat;
    const mensajes = await Chat.find()
                                    .where("_id",id_chat)
                                    .populate("mensajes")
                                    .exec();
    

    res.json({
        ok:true,
        mensajes
    });
});

//Listado de chats.
chatRoutes.get("/",[verificaToken], async (req:any,res:Response)=>{
   const chats= await Chat.find().exec()
   .catch(err =>{res.json({ok:false,err})});

   res.json({
       ok:true,
       chats
   })
});



chatRoutes.post("/mensajes", [verificaToken], (req:any, res:Response)=>{

    const body = req.body;
    body.usuario = req.usuario._id;         //la información del usuario se ha obtenido al decodificar el token.

    const mensaje = {
        usuario: body.usuario,
        texto: body.texto,
    }

    Mensaje.create(mensaje).then( mensajeDB =>{ // Guardamos el mensaje en la base de datos 
        
        Chat.updateOne({ _id: body.chat }, { mensajes:mensajeDB._id }).then( chatDB=>{   //Añadimos el mensaje al chat correspondiente
            res.json({
                ok:true,
                chatDB,
                mensajeDB
            })
        }).catch(err=>{
            res.json({ok:false,err})
        });
    }).catch(err=>{
        res.json({ok:false,err})
    })
    
});

chatRoutes.post("/mensajes/eliminar", [verificaToken], (req:any, res:Response)=>{

    const mensaje = {
        _id:req.body.id
    };

    Mensaje.deleteOne(mensaje).then( deleteDB =>{

        res.json({
            ok:true,
            deleteDB
        })
    }).catch(err=>{ res.json({ok:false,err})});
});

chatRoutes.post("/",[verificaToken],(req:any, res:Response)=>{
    const body = req.body;
    body.usuario = req.usuario._id;         //la información del usuario se ha obtenido al decodificar el token.

   
    const chat = {
        titulo:body.titulo,
        viajes:body.viajes,
        usuarios:body.usuarios
    }

    chat.usuarios.push(body.usuario);

    Chat.create(chat).then( chatDB =>{
        res.json({
            ok:true,
            chatDB
        })
    }).catch(err=>{
        res.json({ok:false,err})
    })
    
    
});

chatRoutes.post("/nuevoUsuario", [verificaToken],(req:any,res:Response)=>{
     const usuario = req.body.usuario;



     res.json({
         ok:true,
         usuario
     })
})

export default chatRoutes;