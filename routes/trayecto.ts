import { Router, Request, Response } from 'express';

import { Trayecto } from '../models/viajes/trayecto.model';
import { verificaToken } from '../middlewares/autenticacion';
import { json } from 'body-parser';
import { Frecuencia } from '../models/fechas/frecuencia.model';

const trayectoRoutes = Router();

//Listado de trayecto
trayectoRoutes.get("/",[verificaToken], async (req:any,res:Response)=>{

    const trayecto = await Trayecto.find().exec()
                                .catch(err=>{res.json({ok:false,err})});

    res.json({ok:true,trayecto});
});

//Creamos un trayecto
trayectoRoutes.post("/", [verificaToken], (req:any, res:Response)=>{
    const body = req.body;
    body.usuario = req.usuario._id;         //la información del usuario se ha obtenido al decodificar el token.

       const trayecto ={
        viajes:body.viajes,
        usuario:body.usuario,
        frecuencia:body.frecuencia
    };

    Trayecto.create(trayecto).then( async trayectoDB=>{
        res.json({
            ok:true,
            trayectoDB
        });
    }).catch(err=>{
        res.json({ok:false,err});
    });
    
});

/* Obtención de fechas */
//Listado
trayectoRoutes.get('/frecuencia',async(req:any,res:Response)=>{
    var fechas = await Frecuencia.find()
                                 .exec()
                .catch(err=>{res.json({ok:false,err})});

                if(fechas){
                    res.json({
                        ok:true,
                        fechas
                    });
                }else{
                    res.json({
                        ok:false,
                        mensaje:"No existe ningúna fecha"
                    });
                }
    
});


//Listado entre dos fechas
trayectoRoutes.get('/frecuencia/acotado',async(req:any,res:Response)=>{
      
    const body = req.body;


    var fechas = await Frecuencia.find()
                                 .exec()
                .catch(err=>{res.json({ok:false,err})});

                if(fechas){
                    const lista = acotar_fechas(fechas,body.limite_inicio,body.limite_fin);

                   res.json({
                       ok:true,
                       lista
                   });
                }else{
                    res.json({
                        ok:false,
                        mensaje:"No existe ninguna fecha"
                    });
                }
    
});

/*Creción de fechas */
trayectoRoutes.post("/frecuencia",[verificaToken], async(req:any,res:Response)=>{
    
    const body = req.body;
    body.usuario = req.usuario._id;         //la información del usuario se ha obtenido al decodificar el token.

    const frecuencia = {
        fecha_inicio: body.fecha_inicio,
        fecha_fin:body.fecha_fin,
        frecuencia:body.frecuencia,
        dias:body.dias,
        dias_semana:body.dias_semana,
        numero:body.numero,
        dias_cancelados:body.dias_cancelados
    }
    
    Frecuencia.create( frecuencia ).then(frecuenciaDB =>{
        res.json({ok:true,frecuenciaDB});

    }).catch(err=>{res.json({ok:false,err})});
});



function acotar_fechas(elemento:any,limite_inicio:string,limite_fin:string){
    var lista:Array<any>=[];


    for(let i=0; i<elemento.length; i++){
        let date_inicio = new Date(limite_inicio);
        let date_fin = new Date(limite_fin);

        var frecuencia = elemento[i];

        let date_frecuencia_inicio = new Date(frecuencia.fecha_inicio);
        let date_frecuencia_fin = new Date(frecuencia.fecha_fin);

        
        //Comprobamos si la fecha inicio de el elemento es posterior a el limite inicial, si es así lo cambiamos

        if( date_frecuencia_inicio >date_inicio){ 
            date_inicio = new Date( date_frecuencia_inicio );
        }

        //Comprobamos si la fecha fin del elemento es anterior a el limite final, si es así lo cambiamos
        if( date_frecuencia_fin ){
            if( date_frecuencia_fin <date_fin){
                date_fin = new Date( date_frecuencia_fin );
            }
        }
        var lista_auxiliar:Array<Date> = []; 
        switch(frecuencia.frecuencia.trim()){
            case "no":              //No existe una frecuencia de repetición.
                lista_auxiliar = [];//array auxiliar para introducir las fechas
                
                frecuencia.dias.forEach((element:any) => {          //Comprobamos si las fechas se encuentran dentro del periodo deseado.
                    let fecha = new Date(element);
                    if(fecha < date_fin && fecha > date_inicio){
                        lista_auxiliar.push(fecha);
                    }
                });
                
                 lista.push(lista_auxiliar);
            break;
            case "día":             //Se repite cada {numero} de días
                 
                var date_aux = new Date(date_inicio);         //Fecha auxiliar 
                lista_auxiliar = [];     //array auxiliar para introducir las fechas


                //Desde la fecha de inicio hasta la fecha fin vamos apuntando los días distanciados por {numero}
                
                while(date_aux <= date_fin){
                    var date = new Date(date_aux);
                    lista_auxiliar.push(date);                         //Introducimos las fechas resultantes en el array auxiliar.
                    date_aux.setDate(date_aux.getDate() + frecuencia.numero);       //Añadimos frecuencia.numero a la fecha auxiliar
                    
                    
                }
                if(frecuencia.dias_cancelados.length){
                    lista_auxiliar = quitar_dias_cancelados(lista_auxiliar, frecuencia.dias_cancelados);
                }
                lista.push(lista_auxiliar);                                              //introducimos la lista auxiliar en la lista final.                               

                break;
            case "semana":          //dias semanas indicados (lunes,martes,miercoles...)


               const fecha_inicio = date_inicio;                //Fecha de inicio
               const fecha_fin = date_fin;                      //fecha fin
               lista_auxiliar = [];                             //array auxiliar para introducir las fechas
                const dias_semana = frecuencia.dias_semana;     //Dias de la semana que queremos
                
                dias_semana.forEach((dia:number) => {
                    
                    let val=0;                           // valor que le tienes que sumar a la fecha para que te de el día (miercoles,lunes...) desaddo.
                    if(fecha_inicio.getDay()> dia){     //Si el día de la semana de la fecha inicio es mayor al día de la semana deseado hay que irse a la semana siguiente 
                        val = 7-fecha_inicio.getDay() + dia;
                    }else{                              //Si el día de la semana de la fecha inicio es menor o igual al día de la semana deseado se calcula la diferencia entre las dos fechas y se la sumamos a la fecha inicial
                        val = dia - fecha_inicio.getDay();
                    }
                    
                    let fecha_auxiliar = new Date(fecha_inicio); 
                    fecha_auxiliar.setDate(fecha_inicio.getDate()  + val);       //Añadimos la diferencia calculada a la fecha auxiliar
                   
                    
                    while(fecha_auxiliar <= fecha_fin){
                        lista_auxiliar.push(new Date(fecha_auxiliar));           //Introducimos los valores en la lista
                        fecha_auxiliar.setDate(fecha_auxiliar.getDate() +7);
                    }

                });
                
                if(frecuencia.dias_cancelados.length){
                    lista_auxiliar = quitar_dias_cancelados(lista_auxiliar, frecuencia.dias_cancelados);
                }


                lista.push(lista_auxiliar);
                    

                break;
            case 'mes':             // Se repite cada x día de mes, el de la fecha de inicio guardado en la db


                let dia_auxiliar = date_frecuencia_inicio.getDate();             //Día inicial, este día de mes es el que nos interesa.
                let dia_inicial = new Date(date_inicio);                         //El primer día que pedimos las fechas.
                
                if(dia_inicial.getDate() > dia_auxiliar){                        //si el dia inicial es mayor que el dia auxiliar hay que avanzar un mes
                    dia_inicial.setMonth(dia_inicial.getMonth() + 1); 
                };

                dia_inicial.setDate(dia_auxiliar);                              //le ponemos a nuestra fecha inicial el día correcto
                lista_auxiliar = [];
               
                
                while(dia_inicial < date_fin){                                  //mientras no nos pasemos de la fecha fin lo vamos añadienedo al listado auxiliar 

                    var fecha_introducir = new Date(dia_inicial);             //Creamos una nueva fecha para que no estén realacionadas.
                    lista_auxiliar.push(fecha_introducir);
                   
                    //Después de introducir la fecha hacemos los cambios para el siguiente mes
                    var mes_siguiente = dia_inicial.getMonth()+1; 
                    dia_inicial.setMonth(mes_siguiente);                    //Creamos la fecha del mes siguiente
                    
                    if(dia_auxiliar != dia_inicial.getDate()){              //En el caso de que se introduzca el día 31,30,29 hay meses que no tienen este día por lo que hay que comprobar que no haya efectos extraños
                        dia_inicial.setDate(dia_auxiliar);
                    }

                    

                }
                
                if(frecuencia.dias_cancelados.length){
                    lista_auxiliar = quitar_dias_cancelados(lista_auxiliar, frecuencia.dias_cancelados);
                }

                lista.push(lista_auxiliar);
                break;
            case 'semana_mes':      //Introducimos en dias_semana[0] el dia de la semana interesado y en numero el 1º,2º,3º de mes 
                let fecha_queremos = new Date(date_inicio);                       //día que nos interesa
                lista_auxiliar = [];
                
                let dia_semana = frecuencia.dias_semana[0];                 //dia semanal que queremos que se repite (lunes martes miercoles...)
                let numero = frecuencia.numero;                             //el 1º,2º,3º martes/miercoles lo que sea 
                
                
                while(fecha_queremos<= date_fin){
                    let primer_dia_mes = new Date(fecha_queremos.setDate(1)).getDay();  //dia de la semana en el que empieza el mes.

                    var dia_queremos =0;
                    if(primer_dia_mes > dia_semana){
                        var primer_domingo = 7-primer_dia_mes+1;                        // obtenemos el numero del día del primer domingo del mes
                        dia_queremos = primer_domingo + dia_semana + 7*(numero -1);     //obtenemos el día que queremos
                    }else{
                        dia_queremos = dia_semana - primer_dia_mes + 7*(numero -1) + 1; //obtenemos el día que queremos
                    }
                    

                    fecha_queremos.setDate(dia_queremos);
                    if(fecha_queremos >= date_inicio && fecha_queremos<= date_fin){
                        lista_auxiliar.push(new Date(fecha_queremos));
                    }

                    fecha_queremos.setMonth(fecha_queremos.getMonth()+1);
                 

                }

                if(frecuencia.dias_cancelados.length){
                    lista_auxiliar = quitar_dias_cancelados(lista_auxiliar, frecuencia.dias_cancelados);
                }

                lista.push(lista_auxiliar);

                break;
            case 'anio':            //con la fecha inicial indicamos que dia del año queremos que se repita
                var dia = new Date(frecuencia.fecha_inicio);
                lista_auxiliar = [];
                while(dia < date_fin ){
                    if(dia >= date_inicio){
                        lista_auxiliar.push(new Date(dia));
                    }
                    dia.setFullYear(dia.getFullYear()+1);
                }

                if(frecuencia.dias_cancelados.length){
                    lista_auxiliar = quitar_dias_cancelados(lista_auxiliar, frecuencia.dias_cancelados);
                }

                lista.push(lista_auxiliar);
                break;
            default:
                lista.push("error");
            break;
            
        }
        
    }
    
    return lista;
};

function quitar_dias_cancelados( array:Array<Date>,dias_cancelados:Array<Date>){
    dias_cancelados.forEach(dia => {
       let pos = array.indexOf(dia);
        if (pos!==-1){
            array.splice(pos,1);
        }
    });

    return array;
}
export default trayectoRoutes;