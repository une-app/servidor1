import { Router, Response } from 'express';
import { verificaToken } from '../middlewares/autenticacion';

import { Viaje } from '../models/viajes/viaje.model';
import { Frecuencia } from '../models/fechas/frecuencia.model';


const viajeRoutes = Router();


/* Metodos GET, obtención de información */


//Obtener Viaje paginados o un viaje unico, peticion publica porque no se verifica el token.
viajeRoutes.get('/listado', async (req: any, res: Response)=>{
    
        let pagina = Number(req.query.pagina) || 1;                     //Obtengo el valor introducido por la url en la variable página, si no hay voy a la página 1
        let skip = (pagina-1)*10;                                       //Obtengo el valor del número de registros que tengo que saltarme para llegar a la página indicada 

        const posts = await Viaje.find()                                 // obtenemos todos los posts registrados en la base de datos
                                .sort({_id: -1})                        //Los ordenamos de más recientes a más antiguos por el id. Cuanto más reciente mayor es el id
                                .skip( skip )                           //Me salto los registros necesarios para llegar a los de la página indicada.
                                .limit( 10 )                            // muestro máximo 10 registros
                                .populate('conductor', '-password')      //introduzco la información de los conductores uniendo ambas dos informaciones
                                .populate('pasajeros', '-password')      //introduzco la información de los pasajeros uniendo ambas dos informaciones
                                .exec();                                //ejecutamos la petición.
        res.json({
            ok:true,
            pagina,
            posts
        });
    
});

//Obtener viajes en los que soy conductor
viajeRoutes.get('/conductor', [ verificaToken ],async(req:any,res:Response)=>{
    
    let id = req.usuario;
    const viajes = await Viaje.find()                                 // obtenemos todos los posts registrados en la base de datos
                    .sort({_id: -1})                                  //Los ordenamos de más recientes a más antiguos por el id. Cuanto más reciente mayor es el id
                    .where('conductor',id)
                    .populate('conductor','-password')
                    .populate('pasajeros','-password')
                    .exec();         

    res.json({
        ok:true,
        viajes
    });
    
})
//Obtengo los viajes en los que soy pasajero
viajeRoutes.get('/pasajero', [ verificaToken ],async(req:any,res:Response)=>{

    let id = req.usuario._id;
    
    var viajes = await Viaje.find()
                        .where('pasajeros',id)
                        .exec()
    .catch(err=>{
        res.json({
            ok:false,err
        });
    });
    

    res.json({
        ok:true,
        viajes
    });
    

});

//Obtener viaje con su id
viajeRoutes.get('/:viajeId', async(req:any,res: Response)=>{
   
        let viaje = req.params.viajeId

        const viajeDB = await Viaje.find()
                                .where("_id",viaje)
                                .populate('conductor','-password')
                                .exec()
        .catch(err=>{
            res.json({
                ok:false,
                err
            });
        });
        if(viajeDB){         //En el caso de que haya resultado.
            res.json({
                ok:true,
                viaje,
                viajeDB
            });

        }else{                      //Si no hay resultado
            res.json({
                ok:false,
                mensaje:"No existe este viaje en nuestra base de datos."
            })
        }
});







 
/* Metodos POST inserción de información */

//Crear Viaje
viajeRoutes.post('/', [ verificaToken ], (req: any, res: Response)=>{
    
    const body = req.body;
    body.usuario = req.usuario._id;         //la información del usuario se ha obtenido al decodificar el token.

    //obtienes la información que entra por el request. lo guardas en la variable user
    const viaje = {
        origen:body.origen,
        destino:body.destino,
        hora_origen:new Date(body.hora_origen),
        hora_destino:new Date(body.hora_destino),
        conductor:body.usuario,
        vehiculo:body.vehiculo,
        pasajeros:body.pasajeros,
        chat:body.chat,
    }


    //guardamos la información en la base de datos
    Viaje.create( viaje ).then( async viajeDB =>{

        //Hace una mezcla de información e introduce la información del usuario en la respuesta.
        await viajeDB.populate('conductor', '-password').execPopulate();   //añadiendo el async, y poniendo el await esperamos a que nos devuelva la información del usuario.
                                                                        //el password no lo queremos, por eso ponemos el segundo argumento
        await viajeDB.populate('pasajeros','-password').execPopulate();
        res.json({
            ok: true,
            post: viajeDB
        })

        }).catch(err =>{

            res.json({
                ok:false,
                err
            });
    
        });
    

});

//Editar Viaje 
viajeRoutes.post('/editar', [verificaToken],async(req:any, res:Response)=>{

    const body = req.body;
    body.usuario = req.usuario._id;         //la información del usuario se ha obtenido al decodificar el token.

    
    //obtienes la información que entra por el request. lo guardas en la variable user
   
    Viaje.findByIdAndUpdate( body._id, body, { new:true }, (err,viajeDB)=>{

        if( err ) return res.json({ok:false,err});

        if( !viajeDB ){
            return res.json({
                ok:false,
                mensaje: 'No existe un viaje con ese ID'
            });
        }
       
       res.json({
           ok:true,
           token: viajeDB 
       });

    });
    

    
});

//Eliminar Viaje
viajeRoutes.post('/eliminar',[ verificaToken ], async(req:any, res:Response)=>{
    const body = req.body;
    body.usuario = req.usuario._id;         //la información del usuario se ha obtenido al decodificar el token.

    //Comprobamos si existe este viaje en la base de datos.
    const viaje = await Viaje.find()
            .where("_id",body._id)
            .where("conductor",body.usuario)        //Comprobamos que el que edita la información es el conductor.
            .remove()
    .catch( err=>{

        res.json({
            ok:false,
            err
        }); 
    });

    res.json({
        ok:true,
        mensaje:"Elemento eliminado."
    });
});



export default viajeRoutes; 