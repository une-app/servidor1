import Server from './classes/server';
import mongoose from 'mongoose';

import cors from 'cors';

import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';

import userRoutes from './routes/usuarios';
import viajeRoutes from './routes/viaje';
import trayectoRoutes from './routes/trayecto';
import chatRoutes from './routes/chat';


const server = new Server();

// Body parser 
server.app.use( bodyParser.urlencoded( { extended:true } ));
server.app.use( bodyParser.json());


//fileUpload
server.app.use( fileUpload( {useTempFiles:true}) );

//Configruar CORS
server.app.use( cors({origin:true,credentials:true}));

//Rutas de mi app
server.app.use( '/user', userRoutes );
server.app.use( '/viaje', viajeRoutes ); 
server.app.use( '/trayecto', trayectoRoutes);
server.app.use( '/chat', chatRoutes);
   
//Conectar DB
mongoose.connect('mongodb://localhost:27017/servidor',  //nombre de la base de datos.
                {useNewUrlParser:true,useCreateIndex: true, useUnifiedTopology:true, useFindAndModify: false },
                (err)=>{
                    if(err) throw err;                      //si hay error muestra el error por consola.
                    console.log('Base de datos online');    //si no hay error saca este mensaje
                });

server.start( () => {
    console.log(`Servidor corriendo en puerto ${server.port}`); //las comillas son tildes.

})