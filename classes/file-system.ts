import { FileUpload } from '../interfaces/file-upload';

import path from 'path';
import fs from 'fs';
import uniqid from 'uniqid'; //hay que importarlo con npm install uniqid y despues 'npm install @types/uniqid --save-dev'


export default class FileSystem {

    constructor() {};

    guardarImagenTemporal( file: FileUpload, userId:string){
        return new Promise( (resolve, reject) =>{
            //creamos la carpeta del usuario
            const path = this.crearCarpetaUsuario( userId );  
    
            //Nombre archivo
            const nombreArchivo = this.generarNombreUnico( file.name );
    
            // Mover el archivo del Temp a nuestra carpeta usuario/temp
            file.mv( `${path}/${nombreArchivo}`, (err:any) =>{
                if ( err ) {
                    reject(err);
                } else{
                    resolve();
                }
            });

        })
    }

    private generarNombreUnico( nombreOriginal:string){
        
        const nombreArr = nombreOriginal.split('.');        //separamos el nombre por puntps
        const extension = nombreArr[nombreArr.length -1];   //la ultima posición del array contendrá la extensión
        
        const idUnico = uniqid();

        return `${ idUnico}.${ extension}`; 
    }
    private crearCarpetaUsuario( userId: string){


        //la variable __dirname te dice el directorio exacto en el ue te encuentras actualmente
        const pathUser = path.resolve( __dirname, '../uploads/', userId );
        const pathUserTemp =path.resolve(pathUser +'/temp'); 

        const existe = fs.existsSync( pathUser );
        if ( !existe ){
            
            fs.mkdirSync( pathUser );
            fs.mkdirSync( pathUserTemp );

        }

        return pathUserTemp;
    }
    public imagenesDeTempHaciaPost( userId: string){

        const pathTemp = path.resolve( __dirname, '../uploads/', userId, 'temp' );
        const pathPost = path.resolve( __dirname, '../uploads/', userId, 'posts' );
        
        if( !fs.existsSync(pathTemp)){
            return[];
        }
        if( !fs.existsSync(pathPost)){
            fs.mkdirSync(pathPost);
        }

        const imagenesTemp = this.obtenerImagenesEnTemp( userId );

        imagenesTemp.forEach( imagen =>{
            fs.renameSync( `${pathTemp}/${ imagen}`,`${pathPost}/${ imagen}`);
        });

        return imagenesTemp;
    }

    private obtenerImagenesEnTemp( userId:string ){
       
        const pathTemp = path.resolve( __dirname, '../uploads/', userId, 'temp' );
        return fs.readdirSync( pathTemp ) || []; 
    }

    public getFotoUrl(userId:string, img:string){
        //crear el path de posts
        const pathFoto = path.resolve( __dirname, '../uploads/', userId, 'posts', img );
        //Comprobamos si la imagen existe
        const existe = fs.existsSync(pathFoto);
        
        //return del pathFoto
        if(!existe){
            return path.resolve( __dirname, '../assets/original.jpg'); 
        }
    }

}