import {Schema, Document,model} from 'mongoose' //cuidado con la primera letra de model, si la pones en mayuscula da error.s
import { Viaje } from './viaje.model';


const trayectoSchema = new Schema({
    created: {
        type:Date
    },
    viajes_conductor:[
        {
        type:Schema.Types.ObjectId,
        ref:Viaje
        }],
    viajes_usuario:[{
        type:Schema.Types.ObjectId,
        ref:Viaje
    }],
    usuario:{
        type:Schema.Types.ObjectId,
        ref:'Usuario',
        required: [true,'Debe existir un usuario del viaje']
    },
    frecuencia:{
        type:Schema.Types.ObjectId,
        ref:'frecuencia',
        required:[true, 'Es necesario indicar al frecuencia en el viaje.']
    }
});


trayectoSchema.pre<ITrayecto>('save', function( next ){
    this.created = new Date();
    next(); //si no se llama a esto no continua la inserción
});

interface ITrayecto extends Document{
    created:Date,
    viajes:Array<Schema.Types.ObjectId>,
    usuario:Schema.Types.ObjectId,
    frecuencia:Schema.Types.ObjectId
}

export const Trayecto = model<ITrayecto>('Trayecto', trayectoSchema);