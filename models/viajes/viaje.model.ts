import {Schema, Document,model} from 'mongoose' //cuidado con la primera letra de model, si la pones en mayuscula da error.
import { Usuario } from '../usuario.model';

const viajeSchema = new Schema({
    created: {
        type:Date
    },
    origen:{
        type:String,             //cambiar como Schema.Types.ObjectId
        required:[true,'Es necesario introducir el lugar de origen']
    },
    destino:{
        type:String,
        required:[true,'Es necesario introducir el lugar de destino']
    },
    hora_origen:{
        type:Date,
        require:[true, 'Es necesario introducir la hora de origen']
    },
    hora_destino:{
        type:Date,
        required:[true,'Es necesario introducir la hora de destino']
    },
    conductor:{
        type:Schema.Types.ObjectId,
        ref:'Usuario',
        required: [true,'Debe existir un conductor en el viaje']
    },
    vehiculo:{

    },
    chat:{
        type:Schema.Types.ObjectId,
        ref:'Chat'
    },
    pasajeros:[{          
        type:Schema.Types.ObjectId,
        ref:Usuario
    }]
});


viajeSchema.pre<IViaje>('save', function( next ){
    this.created = new Date();
    next(); //si no se llama a esto no continua la inserción
});

interface IViaje extends Document {
    created: Date;
    origen:String;
    destino:String;
    hora_origen:Date;
    hora_destino:Date,
    conductor:Schema.Types.ObjectId,
    vehiculo:String,
    pasajeros:Array<Object>,
    chat:Schema.Types.ObjectId
    usuarios:Array<Schema.Types.ObjectId>

}

export const Viaje = model<IViaje>('Viaje', viajeSchema);