import { Schema, Document, model } from "mongoose";
import { Usuario } from '../usuario.model';
import { Viaje } from '../viajes/viaje.model';
import { Mensaje } from './mensaje.model';

const chatSchema = new Schema({
        usuarios:[{
            type:Schema.Types.ObjectId,
            ref:Usuario
        }],
        mensajes:[{
            type:Schema.Types.ObjectId,
            ref:Mensaje
        }],
        titulo:{
            type:String
        }
})

interface IChat extends Document{
    users:Array<Schema.Types.ObjectId>,
    mensajes:Array<Schema.Types.ObjectId>,
    titulo:String,
    viajes:Array<Schema.Types.ObjectId>
}

export const Chat = model<IChat>('Chat', chatSchema);