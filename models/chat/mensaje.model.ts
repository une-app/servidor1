import { Schema, Document, model } from "mongoose";

const mensajeSchema = new Schema({
        usuario:{
            type:Schema.Types.ObjectId,
            required:[true,"Es necesario el usuario del mensaje"]
        },
        texto:{
            type:String,
            required: [true, 'El texto es necesario.']
        },
        fecha:{
            type:Date,
        }
})

mensajeSchema.pre<IMensaje>('save', function( next ){
    this.fecha = new Date();
    next(); //si no se llama a esto no continua la inserción
});

interface IMensaje extends Document{
    usuario:Schema.Types.ObjectId,
    texto:String,
    fecha:Date
}
export const Mensaje = model<IMensaje>('Mensaje', mensajeSchema);