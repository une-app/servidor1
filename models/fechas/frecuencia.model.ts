import {Schema, Document,model} from 'mongoose' //cuidado con la primera letra de model, si la pones en mayuscula da error.s
const frecuenciaSchema = new Schema({
    fecha_inicio:{
        type:Date
    },
    fecha_fin:{
        type:Date
    },
    frecuencia:{         //tipo de frecuencia, por día, por semana, mes o año
        type:String
    },
    dias:[{
        type:Date
    }],
    dias_semana:[{       //dias de la semana (L,M,X,J,V,S,D)
        type:Number
    }],
    numero:{            //nos sirve para el día que se repite de cada mes o el 1º,2º,3º o 4º día de mes y cada cuantos días se repite si es por días. 
        type:Number
    },
    dias_cancelados:[{
        type:Date
    }]
});

frecuenciaSchema.pre<IFrecuencia>('save', function( next ){
    this.fecha_inicio = new Date();
    next(); //si no se llama a esto no continua la inserción
});



interface IFrecuencia extends Document{
    fecha_inicio:Date,
    fecha_fin:Date,
    frecuencia: 'no'|'día' | 'semana' | 'mes' | 'año',
    dias:Array<Date>,
    dias_semana: Array<0|1|2|3|4|5|6>,            //0 indica domingo, 1 lunes, 2 martes...
    numero: number,                             //cada cuanto se reptie 
    dias_cancelados:Array<Date>,
    fechas:Function
}

export const Frecuencia = model<IFrecuencia>('Frecuencia',frecuenciaSchema);