"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const bcrypt_1 = __importDefault(require("bcrypt"));
const token_1 = __importDefault(require("../classes/token"));
const usuario_model_1 = require("../models/usuario.model");
const autenticacion_1 = require("../middlewares/autenticacion");
const userRoutes = express_1.Router();
//Listado de usuarios
userRoutes.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var usuario = yield usuario_model_1.Usuario.find().exec()
        .catch(err => {
        res.json({
            ok: false,
            err
        });
    });
    res.json({
        ok: true,
        usuario
    });
}));
//Información de mi usuario
userRoutes.get('/yo', [autenticacion_1.verificaToken], (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const usuario = req.usuario;
    const informacion = yield usuario_model_1.Usuario.findById(usuario).exec();
    res.json({
        ok: true,
        informacion
    });
}));
//Devolver usuario por id
userRoutes.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = req.params.id;
    const usuario = yield usuario_model_1.Usuario
        .findById(id)
        .exec();
    res.json({
        ok: true,
        usuario
    });
}));
//Login
userRoutes.post('/login', (req, res) => {
    const body = req.body;
    usuario_model_1.Usuario.findOne({ correo: body.correo }, (err, userDB) => {
        if (err)
            throw err;
        if (!userDB) {
            return res.json({
                ok: false,
                mensaje: 'usuario/contraseña no son correctos no correo'
            });
        }
        if (userDB.compararPassword(body.password)) {
            const tokenUser = token_1.default.getJwtToken({
                _id: userDB._id,
                nombre: userDB.nombre,
                correo: userDB.correo,
                foto: userDB.foto
            });
            res.json({
                ok: true,
                token: tokenUser
            });
        }
        else {
            return res.json({
                ok: false,
                mensaje: 'usuario/contraseña no son correctos ***'
            });
        }
    });
});
//Crear un usuario
userRoutes.post('/create', (req, res) => {
    //obtienes la información que entra por el request. lo guardas en la variable user
    const user = {
        nombre: req.body.nombre,
        apellidos: req.body.apellidos,
        correo: req.body.correo,
        password: bcrypt_1.default.hashSync(req.body.password, 10),
        foto: req.body.foto,
        telefono: req.body.telefono
    };
    // --mongoose documentation--
    /**
    * Shortcut for saving one or more documents to the database. MyModel.create(docs)
    * does new MyModel(doc).save() for every doc in docs.
    * Triggers the save() hook.
    */
    //Guardas la variable en la base de datos y te devuelve userDB (la informacion que esta en la base de datos)
    usuario_model_1.Usuario.create(user).then((userDB) => {
        //Transformas a los usuarios en el token correspondiente.
        const tokenUser = token_1.default.getJwtToken({
            _id: userDB._id,
            nombre: userDB.nombre,
            apellidos: req.body.apellidos,
            correo: userDB.correo,
            foto: userDB.foto,
            localidad: userDB.localidad,
            telefono: req.body.telefono
        });
        //Devuelves el ok y el token del usuario.
        res.json({
            ok: true,
            token: tokenUser
        });
    }).catch(err => {
        res.json({
            ok: false,
            err
        });
    });
});
// Actualizar usuario
userRoutes.post('/update', [autenticacion_1.verificaToken], (req, res) => {
    const user = {
        nombre: req.body.nombre || req.usuario.nombre,
        apellidos: req.body.apellidos || req.usuario.apellidos,
        correo: req.body.correo || req.usuario.correo,
        foto: req.body.foto || req.usuario.foto,
        localidad: req.body.localidad || req.usuario.localidad,
        telefono: req.body.telefono || req.usuario.telefono,
        informacion: req.body.informacion || req.usuario.informacion,
        vehiculos: req.body.vehiculos || req.usuario.vehiculos
    };
    usuario_model_1.Usuario.findByIdAndUpdate(req.usuario._id, user, { new: true }, (err, userDB) => {
        //si se updatea con un correo o telefono duplicado mandamos el error.
        if (err) {
            if (err.codeName == 'DuplicateKey') {
                return res.json({
                    ok: false,
                    mensaje: err.errmsg
                });
            }
            else {
                throw err;
            }
            ;
        }
        ;
        if (!userDB) {
            return res.json({
                ok: false,
                mensaje: 'No existe un usuario con ese ID'
            });
        }
        const tokenUser = token_1.default.getJwtToken({
            _id: userDB._id,
            nombre: userDB.nombre,
            correo: userDB.correo,
        });
        res.json({
            ok: true,
            token: tokenUser
        });
    });
});
userRoutes.get('/', [autenticacion_1.verificaToken], (req, res) => {
    const usuario = req.usuario;
    //si la información es correcta devolvemos la información del usuario
    res.json({
        ok: true,
        usuario
    });
});
exports.default = userRoutes;
