"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const autenticacion_1 = require("../middlewares/autenticacion");
const viaje_model_1 = require("../models/viajes/viaje.model");
const file_system_1 = __importDefault(require("../classes/file-system"));
const viajeRoutes = express_1.Router();
const fileSystem = new file_system_1.default();
//Obtener Viaje paginados, peticion publica porque no se verifica el token
viajeRoutes.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let pagina = Number(req.query.pagina) || 1; //Obtengo el valor introducido por la url en la variable página, si no hay voy a la página 1
    let skip = (pagina - 1) * 10; //Obtengo el valor del número de registros que tengo que saltarme para llegar a la página indicada 
    const posts = yield viaje_model_1.Viaje.find() // obtenemos todos los posts registrados en la base de datos
        .sort({ _id: -1 }) //Los ordenamos de más recientes a más antiguos por el id. Cuanto más reciente mayor es el id
        .skip(skip) //Me salto los registros necesarios para llegar a los de la página indicada.
        .limit(10) // muestro máximo 10 registros
        .populate('usuario', '-password') //introduzco la información de los usuarios uniendo ambas dos informaciones
        .exec(); //ejecutamos la petición.
    res.json({
        ok: true,
        pagina,
        posts
    });
}));
//Crear Viaje
viajeRoutes.post('/', [autenticacion_1.verificaToken], (req, res) => {
    const body = req.body;
    body.usuario = req.usuario._id; //la información del usuario se ha obtenido al decodificar el token.
    const imagenes = fileSystem.imagenesDeTempHaciaPost(req.usuario._id); //variable en la que se guardarán el nombre de las imágenes subidas.S
    body.imgs = imagenes;
    //guardamos la información en la base de datos
    Viaj.create(body).then((postDB) => __awaiter(void 0, void 0, void 0, function* () {
        yield postDB.populate('usuario', '-password').execPopulate(); //añadiendo el async, y poniendo el await esperamos a que nos devuelva la información del usuario.
        //el password no lo queremos, por eso ponemos el segundo argumento
        res.json({
            ok: true,
            post: postDB
        });
    })).catch(err => {
        res.json(err);
    });
});
exports.default = postRoutes;
