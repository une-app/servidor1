"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose"); //cuidado con la primera letra de model, si la pones en mayuscula da error.
const viajeSchema = new mongoose_1.Schema({
    created: {
        type: Date
    },
    origen: {
        type: String,
        required: [true, 'Es necesario introducir el lugar de origen']
    },
    destino: {
        type: String,
        required: [true, 'Es necesario introducir el lugar de destino']
    },
    hora_origen: {
        type: Date,
        require: [true, 'Es necesario introducir la hora de origen']
    },
    hora_destino: {
        type: Date,
        required: [true, 'Es necesario introducir la hora de destino']
    },
    conductor: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Usuario',
        required: [true, 'Debe existir un conductor en el viaje']
    },
    vehiculo: {},
    pasajeros: {
        type: Array
    },
    chat: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Chat',
    }
});
viajeSchema.pre('save', function (next) {
    this.created = new Date();
    next(); //si no se llama a esto no continua la inserción
});
exports.Viaje = mongoose_1.model('Viaje', viajeSchema);
