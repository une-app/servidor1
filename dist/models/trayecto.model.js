"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose"); //cuidado con la primera letra de model, si la pones en mayuscula da error.s
const viaje_model_1 = require("./viajes/viaje.model");
const trayectoSchema = new mongoose_1.Schema({
    created: {
        type: Date
    },
    viajes_conductor: [
        {
            type: mongoose_1.Schema.Types.ObjectId,
            ref: viaje_model_1.Viaje
        }
    ],
    viajes_usuario: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: viaje_model_1.Viaje
        }],
    usuario: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Usuario',
        required: [true, 'Debe existir un usuario del viaje']
    },
    frecuencia: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'frecuencia',
        required: [true, 'Es necesario indicar al frecuencia en el viaje.']
    }
});
trayectoSchema.pre('save', function (next) {
    this.created = new Date();
    next(); //si no se llama a esto no continua la inserción
});
exports.Trayecto = mongoose_1.model('Trayecto', trayectoSchema);
