"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = __importDefault(require("./classes/server"));
const mongoose_1 = __importDefault(require("mongoose"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const express_fileupload_1 = __importDefault(require("express-fileupload"));
const usuarios_1 = __importDefault(require("./routes/usuarios"));
const viaje_1 = __importDefault(require("./routes/viaje"));
const trayecto_1 = __importDefault(require("./routes/trayecto"));
const chat_1 = __importDefault(require("./routes/chat"));
const server = new server_1.default();
// Body parser 
server.app.use(body_parser_1.default.urlencoded({ extended: true }));
server.app.use(body_parser_1.default.json());
//fileUpload
server.app.use(express_fileupload_1.default({ useTempFiles: true }));
//Configruar CORS
server.app.use(cors_1.default({ origin: true, credentials: true }));
//Rutas de mi app
server.app.use('/user', usuarios_1.default);
server.app.use('/viaje', viaje_1.default);
server.app.use('/trayecto', trayecto_1.default);
server.app.use('/chat', chat_1.default);
//Conectar DB
mongoose_1.default.connect('mongodb://localhost:27017/servidor', //nombre de la base de datos.
{ useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, useFindAndModify: false }, (err) => {
    if (err)
        throw err; //si hay error muestra el error por consola.
    console.log('Base de datos online'); //si no hay error saca este mensaje
});
server.start(() => {
    console.log(`Servidor corriendo en puerto ${server.port}`); //las comillas son tildes.
});
